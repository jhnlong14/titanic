import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    title_pattern = r'(Mr\.|Mrs\.|Miss\.)'


    df['Title'] = df['Name'].str.extract(title_pattern)

    grouped = df.groupby('Title')

    missing_values_count = grouped['Age'].apply(lambda x: x.isnull().sum())

    median_ages = grouped['Age'].median().round().astype('Int64')
    
    #df['Age'] = df.groupby('Title')['Age'].apply(lambda x: x.fillna(round(x.median())))

    title_order = ['Mr.', 'Mrs.', 'Miss.']

    summary_tuples = [
        (title, missing_values_count[title], median_ages[title])
        for title in title_order if title in median_ages
    ]

    return summary_tuples